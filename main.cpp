#include <iostream>
#include "Vector.h"

using namespace std;

int main() {	
    double* arr = new double[2];
    arr[0] = 2.8;
    arr[1] = 1.7;
    
    Vector a(arr, 2);
    
    cout << "printVector: " << endl;
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "pushBack: {2.1}" << endl;
    a.pushBack(2.1);
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "pushFront: {2.2}" << endl;
    a.pushFront(2.2);
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "popBack: {2.1}" << endl;
    a.popBack();
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "popFront: {2.2}" << endl;
    a.popFront();
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "insert 1: {2.5, 1}" << endl;
    a.insert(2.5, 1);
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "insert 2: {8.8, 7.7, 6.6} with 2-nd position" << endl;
    double ar[3] = {8.8, 7.7, 6.6};
    a.insert(ar, 3, 2);
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "insert 3: {1.2, 3.4, 5.6} with 1-th position" << endl;
    double ar1[3] = {1.2, 3.4, 5.6};
    Vector vec(ar1, 3);
    a.insert(vec, 1);
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "erase: (1, 3)" << endl;
    a.erase(1, 3);
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "eraseBetween: (1, 3)" << endl;
    a.eraseBetween(1, 3);
    a.printVector();
    cout << endl;
    
    cout << "Capacity: " << a.capacity() << endl;
    cout << "Size: " << a.size() << endl;
    cout << endl;
    
    cout << "LoadFactor: " << a.loadFactor() << endl;
    cout << endl;
    
    delete[] arr;
    
    return 0;
}