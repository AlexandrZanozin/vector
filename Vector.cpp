#include "Vector.h"
#include <iostream>

using namespace std;

//принимаем массив типа Value(double), его размер и coef. Тем самым создаём вектор с этими параметрами
Vector::Vector(const Value* rawArray, const size_t size, float coef): _size(size), _multiplicativeCoef(coef), _capacity(size) {
    _data = new Value[_size];
    for(int i = 0; i < size; i++){
        _data[i] = rawArray[i];
    }
}

//кидаем в конструктор вектор и получаем копию вектора(создаёт копию вектора с помощью другого вектора)
Vector::Vector(const Vector& other) {
    _size = other._size;
    _multiplicativeCoef = other._multiplicativeCoef;
    _data = new Value[other._size];
    _capacity = other._size;
    for(int i = 0; i < other._size; i++) {
        _data[i] = other._data[i];
    }
}


Vector& Vector::operator=(const Vector& other) {
    if (this != &other) {
        delete[] _data;
        _multiplicativeCoef = other._multiplicativeCoef;
        _data = new Value[other._capacity];
        _size = other._size;
        _capacity = other._capacity;
        for(int i = 0; i < other._size; i++){
            _data[i] = other._data[i];
        }
    }
    return *this;
}

Vector::Vector(Vector&& other) noexcept {
    _data = other._data;
    _capacity = other._capacity;
    _size = other._size;
    _multiplicativeCoef = other._multiplicativeCoef;
    other._data = nullptr;
    other._capacity = 0;
    other._size = 0;
    other._multiplicativeCoef = 2.0f;
}

Vector& Vector::operator=(Vector&& other) noexcept {
    if (this != &other) {
        delete[] _data;
        _data = other._data;
        _size = other._size;
        _capacity = other._capacity;
        other._data = nullptr;
        other._multiplicativeCoef = 2.0f;
    }
    return *this;
}

void Vector::printVector(){
    for (int i = 0; i < _size; i++){
        cout << " { " << _data[i] << " } ";
    }
    cout << endl;
}

Vector::~Vector() {
    delete[] _data;
}

//push_back	Добавляет элемент в конец вектора
//yes
void Vector::pushBack(const Value& value){
    if(_capacity == 0) {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
    if(_size > _capacity) {
        _capacity *= _multiplicativeCoef;
    }
	
    _data[_size] = value;
    _size++;
}

//push_front Добавляет элемент в начало вектора
//yes
void Vector::pushFront(const Value& value){
    if(_capacity == 0) {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
	
    if(_size > _capacity) {
        _capacity *= _multiplicativeCoef;
    }
    for(int i = _size; i > 0; i--) {
        _data[i] = _data[i-1];
    }
    _data[0] = value;
    _size++;
}

//вставляет только value
//yes
void Vector::insert(const Value& value, size_t pos) {
    if(pos >= _size) {
        throw runtime_error("pos >= _size");
    }
    if(_capacity == 0) {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
    _size++;
    if(_size > _capacity) {
        _capacity *= _multiplicativeCoef;
    }
    Value* mass = new Value[_capacity];
    for(int i = 0; i < pos; i++) {
        mass[i] = _data[i];
    }
    mass[pos] = value;
    for(int i = (pos+1); i < (_size+1); i++) {
        mass[i] = _data[i-1];
    }
    delete[] _data;
    _data = mass;
}

//вставляет массив в вектор
//yes
void Vector::insert(const Value* values, size_t size, size_t pos) {
    if(_capacity == 0) {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
    _size = _size + size;
    if(_size > _capacity) {
        _capacity *= _multiplicativeCoef;
    }
    Value* mass = new Value[_capacity];
    for (int i = 0; i < pos; i++) {
        mass[i] = _data[i];
    }
    for (int i = 0; i < size; i++) {
        mass[pos+i] = values[i];
    }
    for (int i = pos + size; i < _size; i++) {
        mass[i] = _data[i - size];
    }
    delete[] _data;
    _data = mass;
}

//вставляет вектор в вектор(берется _data вектора и вставляется в вектор)
void Vector::insert(const Vector& vector, size_t pos) {
    insert(vector._data, vector._size, pos);
}

//yes pop_back удаление элемента с конца
void Vector::popBack() {
    if(_size > 0) {
        _size -= 1;
    }
    else {
        throw runtime_error("_size <= 0");
    }
}

//yes pop_front удаление элемента с начала
void Vector::popFront() {
    if(_size > 0) {
        for(int i = 0; i < _size-1; i++) {
            _data[i] = _data[i+1];
        }
        _size -= 1;
    }
    else {
        throw runtime_error("_size <= 0");
    }
}

void Vector::erase(size_t pos, size_t count) {
    if(_capacity == 0) {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
    if((_size == 0) || (pos > _size)) {
        throw runtime_error("_size == 0 or pos > _size");
    }
    if((pos + count) > _size) {
        count = (_size - pos);
    }
    for(int i = (pos+count); i < _size; i++) {
        _data[i-count] = _data[i];
    }
    _size -= count;
}

void Vector::eraseBetween(size_t beginPos, size_t endPos) {
    erase(beginPos, (endPos-beginPos));
}

void Vector::reserve(size_t capacity) {
    if (capacity <= _capacity) {
        return;
    }
    else {
        Value* mass = new Value[capacity];
        for(int i = 0; i < _size; i++) {
            mass[i] = _data[i]; 
        }
        delete[] _data;
        _data = mass;
        _capacity = capacity;	
    }
}


void Vector::shrinkToFit() {
    if(_capacity > _size) {
        _capacity = _size;
    }
    Value* mass = new Value[_capacity];
    for(int i = 0; i < _size; i++) {
        mass[i] = _data[i];
    }
    delete [] _data;
    _data = mass;
}

size_t Vector::size() const {
    return _size;
}

size_t Vector::capacity() const {
    return _capacity;
}

double Vector::loadFactor() const {
    return (double(_size)/double(_capacity));
}

Value& Vector::operator[](size_t idx) {
    return _data[idx];
}

const Value& Vector::operator[](size_t idx) const {
    return _data[idx];
}

long long Vector::find(const Value& value) const {
    for(int i = 0; i < _size; i++) {
        if(_data[i] == value) {
            return i;
        }
    }
    return 0;
}

//Iterator
Vector::Iterator::Iterator(Value *ptr) {
    _ptr = ptr;
}

Value& Vector::Iterator::operator*() {
    return *_ptr;
}

const Value& Vector::Iterator::operator*() const {
    return *_ptr;
}

Value* Vector::Iterator::operator->() {
    return _ptr;
}

const Value* Vector::Iterator::operator->() const {
    return _ptr;
}

Vector::Iterator Vector::Iterator::operator++() {
    _ptr++;
    return *this;
}

Vector::Iterator Vector::Iterator::operator++(int) {
    ++_ptr;
    return *this;
}

bool Vector::Iterator::operator==(const Iterator& other) const {
    return (_ptr == other._ptr);
}

bool Vector::Iterator::operator!=(const Iterator& other) const {
    return !(_ptr == other._ptr);
}

Vector::Iterator Vector::begin() {
    return Iterator(_data);
}

Vector::Iterator Vector::end() {
    return Iterator(_data+_size);
}
